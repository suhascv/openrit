import './App.scss';
import Navigation from './components/navigation.js';
import About from './components/about/about.js';
import Home from './components/home/home.js';
import OpenCalendar from'./components/calendar/openCalendar.js';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";



function App() {
  return (
    <div className="App">
      
      <Navigation/>
      <main>
      <Router>
      <Switch>
        <Route exact path="/">
        <Home/>
        
        </Route>
        <Route path="/about">
        <About/>
        </Route>
        <Route path="/calendar">
        <OpenCalendar/>
        </Route>
      </Switch>
      
      </Router>
      </main>
      
    </div>
  );
}

export default App;
