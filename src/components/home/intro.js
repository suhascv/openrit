import './Intro.scss';
import introduction from './introduction.json';
export default function Intro(){
    return(
        <div className="intro">
            <div className="container">
            <h1>Open @ RIT</h1>
            <h3>{introduction.tag}</h3>
            </div>
        </div>
    )
}