import {useEffect } from 'react';
import './Home.scss';
import Intro from './intro.js';
import homeRoutes from './homeRoutes.json';




const GetRoutes=(props)=>{
    return(
    
    <div className="home-routes">
        <h2>{props.title}</h2>
        <ul>
            {props.routes.map((route,index)=>(
                <a key={index} href={route.url}>
                <li>{route.name}</li>
                </a>
            ))}
        </ul>
    </div>
    );
}



export default function Home(){
    
    useEffect(()=>{
        localStorage.setItem('route','/');
    })

    return(
        <div className="home">
            <Intro/>
            
            <div className="home-container">
                <div className="container"> 
                        <div className="content">
                            {homeRoutes.map(hr=>(<GetRoutes key ={hr.title} title={hr.title} routes={hr.routes}/>))}
                            
                        </div>
                </div>
            </div>
            
            
        </div>
    );
}