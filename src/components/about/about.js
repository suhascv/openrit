import './About.scss';


export default function About(){
    return(<div className="about">
            <div className="container">
            <article>
                <div className="about-text">
                    <h1>About</h1>
                    <p>Open@RIT is dedicated to fostering the collaborative engine of Open across the University for Faculty, Staff and Students. Its goals are to discover, and grow, the footprint of RIT’s impact on all things Open including, but not limited to, Open Source Software, Open Data, Open Hardware, Open Educational Resources and Creative Commons licensed efforts, what we like to refer to in aggregate as Open Work. </p>
                    <p></p>
                </div>  
            </article>
            </div>
    </div>)
}