import React from 'react';
import ReactTooltip from 'react-tooltip';
import'./Calendar.scss';
import calendarEvents from './calendarEvents.json';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid';

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c==='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}






export default class OpenCalendar extends React.Component{
constructor(props){
    super(props);
    this.state={
        event: {
            title: 'Sample Event',
            location: 'Portland, OR',
            startTime: '2021-02-16T20:15:00-04:00',
            endTime: '2021-02-16T21:45:00-04:00'
          },
        
        
    }
    this.handleEvent=this.handleEvent.bind(this);
}

handleEvent(info){
    let start = false;
    
    let allInfo = [{"tag":"Title","text":info.event.title},] 
    let eID =create_UUID();
    if(info.event.allDay!==true){
        start=formatAMPM(info.event.start);
        allInfo.push({"tag":"Starts at","text":start})
    }
    if(info.event.extendedProps.description){
        allInfo.push({"tag":"description","text":info.event.extendedProps.description})
    }
    return(
   
    <div className="event-tool" data-tip data-for={eID}>
        <div >
        {info.event.title}
        {info.event.description}
        {start?start:null}
        </div>
    <ReactTooltip id={eID}className="tool-tip" place="bottom" effect="solid" >
    {allInfo.map((comp,index)=><div key={index}>{comp.tag+"  :  "+comp.text}</div>)}
    </ReactTooltip>
    </div>
    )
}


render(){
    return (
    <div className="container">
        <div className="calendar">
        <FullCalendar
            plugins={[ dayGridPlugin ]}
            initialView="dayGridMonth"
            weekends={true}
            events={calendarEvents}
            eventContent ={this.handleEvent}
            />
            
        <ReactTooltip/>
        </div>
    </div>
    );
}

}