import './Navigation.scss';
import Logo from '../images/logo.png';
import navigations from '../data/navagations.json';
import React from 'react';


const setRoute =()=>{

    try{
        localStorage.setItem('display-title','Open@RIT');
        localStorage.setItem('route','/home');
        }
        catch(e){

        }
}

const getRoute = ()=>{
    try{
    return localStorage.getItem('route');
    }
    catch(e){
        return '/home';
    }
}

class Options extends React.Component{
    constructor(props){
        super(props);
        this.state={
            route:"/",
            top:76,
            name:"Open@RIT"
        }
        this.handleRoute = this.handleRoute.bind(this);

    }

    handleRoute(url,title){
        localStorage.setItem('route',url);
        localStorage.setItem('display-title',title);
        
    }

    componentDidMount(){
        //this.setState({top:window.scrollY+76});
        //console.log(this.state.top) ;
        try{
        document.title=localStorage.getItem('display-title');
        }
        catch(e){
            document.title = "Open@RIT";
        }

    }

        render(){

        return(
            <div id={this.props.id} className={this.props.style} >
            {navigations.map(nav=>{ 
                return(
                <a className="animate__animated animate__backInDown animate__fast" key={nav.name} 
                    href={""+nav.url} 
                    style={{color:nav.url===getRoute()?'black':''}}
                    onClick={()=>this.handleRoute(nav.url,nav.name)}
                    
                    >{nav.name}
                    
                </a>
                
            )})
            }
            </div>
        )
    }
}

class Navigation extends React.Component{
    constructor(props){
        super(props);
        this.state={
            display:false,
            bar:'',
            route:'',
            
        };
        this.handleDisplay = this.handleDisplay.bind(this);
        
    }
    handleDisplay(){
        let display=this.state.display;
        this.setState({display:display?false:true});
        this.setState({bar:display?'':'cross'});  
    }



    
    

    render(){
 return(   
     <div className="navbar">
        <header className={"navigation"} >
            <a className="logo" href="/" onClick={ ()=>{setRoute()}
            }>
                <img className="image" src={Logo} alt="RIT"></img>
            </a>
            {<Options id="web-only-options" style="web-only-options"/>}
            
            <div id="mobile-only-bar" className={"mobile-only-bar "+this.state.bar} onClick={this.handleDisplay}>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </header>
        {this.state.display?<Options id="mobile-only-options" />:null} 
    </div>
 );
            } 
}

export default Navigation;